import pandas as pd
from collections import defaultdict
import os
import numpy as np
from sklearn.model_selection import train_test_split
import tensorflow as tf
import tensorflow_hub as hub
from sklearn import preprocessing
from config import GeneralConfig

print("Version: ", tf.__version__)
print("Eager mode: ", tf.executing_eagerly())
print("Hub version: ", hub.__version__)
print("GPU is", "available" if tf.config.experimental.list_physical_devices("GPU") else "NOT AVAILABLE")

my_dir_path="aclImdb/"
def get_dataframe(my_dir_path, folder):
    results = defaultdict(list)
    for sentiment in ['pos', 'neg']:
        full_dir = os.path.join(my_dir_path, folder, sentiment)
        for file in os.listdir(full_dir):
            if file.endswith('txt'):
                with open(os.path.join(full_dir, file), "rb") as file_open:
                    results["file_name"] = file
                    results["text"].append(file_open.read())
                    results["sentiment"].append(sentiment)
    df = pd.DataFrame(results)
    le = preprocessing.LabelEncoder()
    le.fit(df['sentiment'])
    print(le.classes_)
    df['sentiment'] = le.transform(df['sentiment']) 
    return df

    
class SentimentModel:
    def __init__(self):
        self.DIR_PATH = "aclImdb/"
        self.EMBEDDING = GeneralConfig.SENTENCE_ENCODER_MODEL_PATH
        self.model = self._init_model()
        self.df_train = None
    
    def _get_embeddings(self):
        return hub.KerasLayer(self.EMBEDDING, input_shape=[], dtype=tf.string, trainable=True)
            
    def _init_model(self):
        model = tf.keras.Sequential()
        model.add(self._get_embeddings())
        model.add(tf.keras.layers.Dense(16, activation='relu'))
        model.add(tf.keras.layers.Dense(1, activation='sigmoid'))
        model.summary()
        # calculate loss
        model.compile(optimizer='adam',
              loss='binary_crossentropy',
              metrics=['accuracy'])
        return model

    def _get_dataframe(self, folder):
        _path = os.path.join(GeneralConfig.PROJECT_ROOT)
        return get_dataframe(os.path.join(_path, self.DIR_PATH), folder) 
    
    def train(self): 
        df_train = self._get_dataframe("train")
            
        X_train, X_test, y_train, y_test = train_test_split(df_train["text"], 
                                                            df_train["sentiment"],
                                                            test_size=0.3, random_state=42)
        print("data loaded...")
        self.model.fit(X_train.values, 
                y_train.values, 
                batch_size=32, 
                epochs=10)
        print("model fitted...")
        self.model.save_weights('model.h5')
        results = self.model.evaluate(X_test.values, y_test.values, batch_size=32)
        for name, value in zip(self.model.metrics_names, results):
            print("%s: %.3f" % (name, value))
    
    def _get_sentiment(self, message: str):
        message = [message, message]
        predicts = self.model.predict(message)[0]
        sentiment = self._format_results(predicts)
        print(f"Sentiment of '{message[0]}' is {sentiment}.")
        return sentiment

    def _format_results(self, predicts):
        return GeneralConfig.SENTIMENT[(np.where(predicts > 0.5, 1, 0).item())]
    
    def load_weights(self):
        print(GeneralConfig.SENTIMENT_MODEL_PATH)
        return self.model.load_weights(GeneralConfig.SENTIMENT_MODEL_PATH)
    
if __name__ == "__main__":
    sentiment = SentimentModel()
    sentiment.train()
    sentiment._get_sentiment("i hate this film so much it is the worst thing i have ever seen")

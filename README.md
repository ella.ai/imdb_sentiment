# IMDB Classification Task
Using tensorflow2.0 with keras

## Create environment 
`pip install -r requirements.txt`

## Architecture
The model architecture uses a pre-trained text embedding availabe from tensorflow_hub "google/tf2-preview/gnews-swivel-20dim/1" as the first layer. This feeds into a fully connected dense layer with 16 hidden cells which feeds into the last layer which produces a binary output using the softmax activation function.

The hyperparameters were chosen intuitively for initial training. 

Results on the unseen test set are
loss: 0.418
accuracy: 0.869

## Production
Code to get sentiment for production
```
sentiment = SentimentModel() #this loads the model using a lazy load function so only done once
sentiment._get_sentiment("sentence_to_get_sentiment_for")
```

## To train
`python main.py train`

## To test
add list of `messages` then:
`python main.py test`

### If more time
1. use random or grid search to fine tune hyperparameters
2. calculate F1 score to account for true negatives and fale positives
3. experiment with larger pre-trained word embeddings
4. notice the model only works well on long reviews due to that nature of the training set, be interesting to explore further.
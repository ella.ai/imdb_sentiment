import os

class GeneralConfig:
    PROJECT_ROOT = os.path.abspath(os.path.dirname("__file__"))
    SENTIMENT = {0:"negative", 1:"positive"}
    SENTENCE_ENCODER_MODEL_PATH = "https://tfhub.dev/google/tf2-preview/gnews-swivel-20dim/1"
    SENTIMENT_MODEL_PATH = os.path.join(PROJECT_ROOT, 'model.h5')

if __name__=="__main__":
    conf = GeneralConfig()
    conf.PROJECT_ROOT
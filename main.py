import sys
import os
from sentiment import SentimentModel
from argparse import ArgumentParser
import pickle
import pandas as pd
import time

messages = ["test message 1", "test message 2"]
CWD = os.getcwd()

def run_interactively():
    model = SentimentModel()
    model.load_weights()
    
    print("Type exit and press enter to end the conversation.")
    message = input(">")
    while message:
        if message.strip() == 'exit':
            print("Thank you for chatting. Goodbye.")
            break
        start = time.time()
        sentiment = model._get_sentiment(message)
        end = time.time()
        print(sentiment, "Execution Time:", round((end - start), 4))
        message = input(">")
 
def run_training():
    model = SentimentModel()
    model.train()


def run_tests():
    model = SentimentModel()
    model.load_weights()
    for message in messages:
        result = model._get_sentiment(message)
        print(f"{message} | {result}")


if __name__ == '__main__':

    parser = ArgumentParser(description="Run the emotions model")
    parser.add_argument('mode', choices=['run', 'test','train', 'benchmark'], default='run',
                        help='Choose the mode: run, train, or test.')
    args = parser.parse_args()

    if args.mode == 'run':
        run_interactively()

    if args.mode == 'train':
        run_training()

    if args.mode == 'test':
        run_tests()